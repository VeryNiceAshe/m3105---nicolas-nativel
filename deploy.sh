#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns
himage dwikiorg mkdir -p /etc/named
hcp configuration/dwikiorg/named.conf dwikiorg:/etc/.
hcp configuration/dwikiorg/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf 

## configuration de iut.re 
# configuration du serveur dns
himage diutre mkdir -p /etc/named
hcp configuration/diutre/named.conf diutre:/etc/.
hcp configuration/diutre/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 

## configuration de rt.iut.re 
# configuration du serveur dns
himage drtiutre mkdir -p /etc/named
hcp configuration/drtiutre/named.conf drtiutre:/etc/.
hcp configuration/drtiutre/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf 

## configuration de .org
# configuration du serveur dorg
himage dorg mkdir -p /etc/named
hcp configuration/dorg/named.conf dorg:/etc/.
hcp configuration/dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf

## configuration de .re
# configuration du serveur dre
himage dre mkdir -p /etc/named
hcp configuration/dre/named.conf dre:/etc/.
hcp configuration/dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf

## configuration de aRootServer
# configuration du serveur root
himage aRootServer mkdir -p /etc/named
hcp configuration/arootserver/named.conf aRootServer:/etc/.
hcp configuration/arootserver/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf

## configuration de bRootServer
# configuration du serveur root
himage bRootServer mkdir -p /etc/named
hcp configuration/brootserver/named.conf bRootServer:/etc/.
hcp configuration/brootserver/* bRootServer:/etc/named/.
himage bRootServer rm /etc/named/named.conf

## configuration de cRootServer
# configuration du serveur root
himage cRootServer mkdir -p /etc/named
hcp configuration/crootserver/named.conf cRootServer:/etc/.
hcp configuration/crootserver/* cRootServer:/etc/named/.
himage cRootServer rm /etc/named/named.conf

## configuration de pc1
# resolv.conf
hcp configuration/pc1/resolv.conf pc1:/etc/.

## configuration de pc2
# resolv.conf
hcp configuration/pc2/resolv.conf pc2:/etc/.

bash launch_dns.sh
